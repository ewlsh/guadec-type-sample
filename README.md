# GUADEC Types Sample

To setup this project install [`yarn v1.x`](https://classic.yarnpkg.com/en/docs/install#debian-stable).

```bash
yarn
```

I recommend using an editor which has good TypeScript support. VSCode is by far the best out-of-the-box, Atom can be decently configured, and IntelliJ WebStorm has gained fairly decent support in recent years.

Any editor which supports the Language Server Protocol and thus the TypeScript Language Server will give you a fairly robust experience.